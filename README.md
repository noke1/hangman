# Hangman

## Problem

This project is made to learn a liite bit more of JavaScript. I did not care much about the styling or UI/UX as it was not the goal of this exercise.
Used:

-   Vanilla JS
-   Bootstrap
-   Node

## Getting started

1. Download the source files
2. Navigate into the `hangman` directory
3. Open the terminal and run `npm start`
4. Open the browser and go to `https://localhost:7777`. The deault port assigned by the app is `7777`. It can be changed inside the `index.js` script.
5. Have fun

## Game rules

-   To start the game press the `Random!` button. It then creates the random word and stores it inside the sessions storage.
-   You can guess the random word by:
    -   Clicking on the button with letter => If letter is present inside the word the letter will change it's colour to green. If not the the colour changes to red.
    -   Providing the word inside the input field and confirming the guess. Successfull guess ends the game immediately.
-   You have 6 guesses to win the game. Each failed guess (guess method does not matter) increase the guesses counter. After 6th failed attempt the game ends.With each failed guess the next hangman part is drawn :)
