import { getFailedGuessesCountFromSessionStorage } from '../handlers/sessionStorage.js';

export const displayHangmanState = () => {
    let hangman = document.querySelector('#hangman');
    let image = document.createElement('img');
    let currentFailedGuesses = getFailedGuessesCountFromSessionStorage();

    switch (Number(currentFailedGuesses)) {
        case 1:
            image.src = '../images/1.png';
            image.alt = 'frame';
            break;
        case 2:
            image.src = '../images/2.png';
            image.alt = 'body';
            break;
        case 3:
            image.src = '../images/3.png';
            image.alt = 'left hand';
            break;
        case 4:
            image.src = '../images/4.png';
            image.alt = 'right hand';
            break;
        case 5:
            image.src = '../images/5.png';
            image.alt = 'left leg';
            break;
        case 6:
            image.src = '../images/6.png';
            image.alt = 'right leg';
            break;
        default:
            break;
    }
    removeChilds(hangman);
    hangman.appendChild(image);
};

const removeChilds = (parent) => {
    while (parent.firstChild) {
        parent.removeChild(parent.lastChild);
    }
};
