export const displayToast = (text) => {
    const toastText = document.querySelector('#toast-text');
    toastText.innerHTML = text;

    const toastLiveExample = document.getElementById('liveToast');
    const toast = new bootstrap.Toast(toastLiveExample);
    toast.show();
};
