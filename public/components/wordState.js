import { getGuesses } from './alphabet.js';

export const displayGuessedLettersForWord = (wordToCheck) => {
    const guesses = getGuesses();
    let guessed = wordToCheck
        .split('')
        .map((x) => replaceDashWithLetterGuessed(x, guesses))
        .join(' ');

    let dashedWord = document.querySelector('#word-floors');
    dashedWord.innerHTML = guessed;
};

const replaceDashWithLetterGuessed = (letter, array) => {
    return array.includes(letter) ? letter : '_';
};
