import { displayGuessedLettersForWord } from './wordState.js';
import {
    getWordFromSessionStorage,
    increaseFailedGuessesCount,
} from '../handlers/sessionStorage.js';
import {
    isGameWon,
    isFailedGuessesCountReached as isFailedGuessesCountReached,
} from '../handlers/game.js';
import { displayToast } from './toast.js';
import { displayHangmanState } from '../components/hangman.js';
import { disableWordGuessMethod } from './wordGuess.js';
import { disableGetRandomWord } from './randomWord.js';

let guesses = [];

export const createAlphabet = () => {
    const alphabet = 'abcdefghijklmnopqrstuvwxyz';

    alphabet.split('').forEach((letter) => {
        let letterButton = document.createElement('button');
        letterButton.innerHTML = letter;
        letterButton.setAttribute('id', `letter-${letter}`);
        letterButton.classList.add('btn');
        letterButton.classList.add('btn-primary');
        letterButton.classList.add('col-md-1');
        letterButton.classList.add('mx-1');
        letterButton.classList.add('my-1');
        letterButton.addEventListener('click', (event) =>
            checkGuessFromAlphabet(event)
        );

        letters.appendChild(letterButton);
    });
};

export const getGuesses = () => {
    return guesses;
};

const checkGuessFromAlphabet = (event) => {
    let buttonClicked = document.querySelector(`#${event.target.id}`);
    const letter = buttonClicked.innerHTML;
    buttonClicked.disabled = true;

    guesses.push(letter);

    const generatedRandomWord = getWordFromSessionStorage();
    const isLetterPresentInWord = generatedRandomWord.includes(letter);
    if (isLetterPresentInWord) {
        changeColourIfLetterPresent(buttonClicked, 'success');
        displayGuessedLettersForWord(generatedRandomWord);
        if (isGameWon()) {
            displayToast('You won! Please refresh the page to start again.');
            disableAlphabetGuessMethod();
            disableWordGuessMethod();
            disableGetRandomWord();
        }
    } else {
        changeColourIfLetterPresent(buttonClicked, 'danger');
        increaseFailedGuessesCount();
        displayHangmanState();
        if (isFailedGuessesCountReached()) {
            displayToast(
                `The game is lost. Please refresh the page to start again.\n
                And the word was: ${getWordFromSessionStorage()}`
            );
            disableAlphabetGuessMethod();
            disableWordGuessMethod();
            disableGetRandomWord();
        }
    }
};

export const disableAlphabetGuessMethod = () => {
    document
        .querySelectorAll('#letters button')
        .forEach((letter) => (letter.disabled = true));
};

const changeColourIfLetterPresent = (button, colour = 'danger') => {
    const colours = {
        success: 'btn-success',
        danger: 'btn-danger',
    };
    button.classList.remove('btn-primary');
    button.classList.add(colours[colour]);
};
