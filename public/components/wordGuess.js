import { isGameWon, isFailedGuessesCountReached } from '../handlers/game.js';
import { disableAlphabetGuessMethod } from './alphabet.js';
import { displayToast } from './toast.js';
import {
    increaseFailedGuessesCount,
    getWordFromSessionStorage,
} from '../handlers/sessionStorage.js';
import { displayHangmanState } from './hangman.js';
import { disableGetRandomWord } from './randomWord.js';

const guessInput = document.querySelector('#guess input');

export const checkGuessFromInput = () => {
    if (isGameWon()) {
        displayToast('You won! Please refresh the page to start again.');
        disableWordGuessMethod();
        disableAlphabetGuessMethod();
        disableGetRandomWord();
    } else {
        increaseFailedGuessesCount();
        displayHangmanState();
        if (isFailedGuessesCountReached()) {
            displayToast(
                `The game is lost. Please refresh the page to start again.\n
                And the word was: ${getWordFromSessionStorage()}`
            );
            disableWordGuessMethod();
            disableAlphabetGuessMethod();
            disableGetRandomWord();
        } else {
            displayToast('This guess is wrong.');
            guessInput.value = '';
        }
    }
};

export const disableWordGuessMethod = () => {
    const guessButton = document.querySelector('#guess button');
    guessButton.disabled = true;
    guessInput.disabled = true;
};

export const getGuessInput = () => {
    return guessInput;
};
