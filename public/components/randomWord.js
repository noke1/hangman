import { displayToast } from './toast.js';
import { saveToSessionStorage } from '../handlers/sessionStorage.js';
import { displayWordInformation } from './wordInfo.js';
import { getRandomWord } from '../handlers/fetcher.js';

const API_URL = 'https://random-words-api.vercel.app/word';
const buttonGetWord = document.querySelector('#get-word');

export const disableGetRandomWord = () => {
    buttonGetWord.disabled = true;
};

export const prepareTheGame = async () => {
    const randomWord = await getRandomWord(API_URL);

    saveToSessionStorage('randomWord', randomWord.toLowerCase());
    saveToSessionStorage('failedGuesses', 0);
    displayToast('Word generated succesfully!');
    displayWordInformation(randomWord);
};

buttonGetWord.addEventListener('click', prepareTheGame);
