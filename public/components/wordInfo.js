const dashedWord = document.querySelector('#word-floors');

export const displayWordInformation = (word) => {
    const wordInformation = document.querySelector('#word-info');
    const wordLengthInformation = document.querySelector('#word-info > p');

    wordLengthInformation.innerHTML = `Hint: your word has ${word.length} letters.`;

    dashedWord.innerHTML = word
        .split('')
        .map((x) => '_')
        .join(' ');

    removeChildOfType(wordInformation, 'p');

    wordInformation.appendChild(wordLengthInformation);
    wordInformation.appendChild(dashedWord);
};

export const getDashedWord = () => {
    return dashedWord;
};

const removeChildOfType = (parent, child) => {
    if (!parent.querySelectorAll(child).length) {
        throw new Error(`No child elements to remove from ${parent}`);
    }
    while (parent.firstChild) {
        parent.removeChild(parent.lastChild);
    }
};
