import { getGuessInput } from '../components/wordGuess.js';
import { getDashedWord } from '../components/wordInfo.js';
import {
    getFailedGuessesCountFromSessionStorage,
    getWordFromSessionStorage,
} from './sessionStorage.js';

export const isGameWon = () => {
    const guessInput = getGuessInput();
    const dashedWord = getDashedWord().innerHTML.split(' ').join('');
    const randomWord = getWordFromSessionStorage();

    const isWordGuessedByAlphabet = dashedWord === randomWord;
    const isWordGuessedByInput = guessInput.value === randomWord;

    return isWordGuessedByAlphabet || isWordGuessedByInput;
};

export const isFailedGuessesCountReached = () => {
    const maxFailedAttempts = 6;
    const currentFailedAttempts = getFailedGuessesCountFromSessionStorage();

    return currentFailedAttempts >= maxFailedAttempts;
};
