export const getWordFromSessionStorage = () => {
    return sessionStorage.getItem('randomWord');
};

export const getFailedGuessesCountFromSessionStorage = () => {
    return +sessionStorage.getItem('failedGuesses');
};

export const saveToSessionStorage = (key, value) => {
    sessionStorage.setItem(key, value);
};

export const increaseFailedGuessesCount = () => {
    saveToSessionStorage(
        'failedGuesses',
        Number(getFailedGuessesCountFromSessionStorage()) + 1
    );
};
