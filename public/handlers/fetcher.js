import { displayToast } from '../components/toast.js';
import { disableAlphabetGuessMethod } from '../components/alphabet.js';
import { disableGetRandomWord } from '../components/randomWord.js';
import { disableWordGuessMethod } from '../components/wordGuess.js';

export const getRandomWord = (endpoint) => {
    return fetch(endpoint)
        .then((res) => res.json())
        .then((data) => {
            const { word: randomWord } = data[0];
            return randomWord;
        })
        .catch((error) => {
            displayToast(`Something went wrong. See: ${error}}`);
            disableAlphabetGuessMethod();
            disableWordGuessMethod();
            disableGetRandomWord();
        });
};
