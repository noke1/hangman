import { createAlphabet } from './components/alphabet.js';
import { checkGuessFromInput } from './components/wordGuess.js';

const guessButton = document.querySelector('#guess button');

window.addEventListener('load', createAlphabet);

guessButton.addEventListener('click', checkGuessFromInput);
